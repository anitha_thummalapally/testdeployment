/*Copyright (c) 2016-2017 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.testdeployment.testdefaultvalues;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Test1 generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`test1`")
public class Test1 implements Serializable {

    private Integer a1;

    @Id
    @Column(name = "`a1`", nullable = true, scale = 0, precision = 10)
    public Integer getA1() {
        return this.a1;
    }

    public void setA1(Integer a1) {
        this.a1 = a1;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Test1)) return false;
        final Test1 test1 = (Test1) o;
        return Objects.equals(getA1(), test1.getA1());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getA1());
    }
}

