/*Copyright (c) 2016-2017 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.testdeployment.testdefaultvalues;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * DatabaseDefinedInt generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`DatabaseDefinedInt`")
public class DatabaseDefinedInt implements Serializable {

    private Integer pkCol;
    private Integer intColRenamed;
    private String stringcol;

    @Id
    @Column(name = "`PkCol`", nullable = false, scale = 0, precision = 10)
    public Integer getPkCol() {
        return this.pkCol;
    }

    public void setPkCol(Integer pkCol) {
        this.pkCol = pkCol;
    }

    @Column(name = "`IntColRenamed`", nullable = true, scale = 0, precision = 10)
    public Integer getIntColRenamed() {
        return this.intColRenamed;
    }

    public void setIntColRenamed(Integer intColRenamed) {
        this.intColRenamed = intColRenamed;
    }

    @Column(name = "`Stringcol`", nullable = true, length = 20)
    public String getStringcol() {
        return this.stringcol;
    }

    public void setStringcol(String stringcol) {
        this.stringcol = stringcol;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DatabaseDefinedInt)) return false;
        final DatabaseDefinedInt databaseDefinedInt = (DatabaseDefinedInt) o;
        return Objects.equals(getPkCol(), databaseDefinedInt.getPkCol());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPkCol());
    }
}

