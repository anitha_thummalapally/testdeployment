/*Copyright (c) 2016-2017 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.testdeployment.testdefaultvalues;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

/**
 * ServerDefinedIu generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`Server Defined IU`")
@IdClass(ServerDefinedIuId.class)
public class ServerDefinedIu implements Serializable {

    private Integer integerCol;
    private Long bigIntegerCol;
    private Date dateCol;
    private Time timeCol;
    @Type(type = "DateTime")
    private LocalDateTime dateTimeCol;
    private String userId;
    private String username;
    private String authenticated;
    private Integer intColUserId;

    @Id
    @Column(name = "`Integer Col`", nullable = false, scale = 0, precision = 10)
    public Integer getIntegerCol() {
        return this.integerCol;
    }

    public void setIntegerCol(Integer integerCol) {
        this.integerCol = integerCol;
    }

    @Id
    @Column(name = "`BigInteger Col`", nullable = false, scale = 0, precision = 19)
    public Long getBigIntegerCol() {
        return this.bigIntegerCol;
    }

    public void setBigIntegerCol(Long bigIntegerCol) {
        this.bigIntegerCol = bigIntegerCol;
    }

    @Column(name = "`Date Col`", nullable = true)
    public Date getDateCol() {
        return this.dateCol;
    }

    public void setDateCol(Date dateCol) {
        this.dateCol = dateCol;
    }

    @Column(name = "`Time Col`", nullable = true)
    public Time getTimeCol() {
        return this.timeCol;
    }

    public void setTimeCol(Time timeCol) {
        this.timeCol = timeCol;
    }

    @Column(name = "`DateTime Col`", nullable = true)
    public LocalDateTime getDateTimeCol() {
        return this.dateTimeCol;
    }

    public void setDateTimeCol(LocalDateTime dateTimeCol) {
        this.dateTimeCol = dateTimeCol;
    }

    @Column(name = "`UserId`", nullable = true, length = 255)
    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Column(name = "`Username`", nullable = true, length = 255)
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "`Authenticated`", nullable = true, length = 1)
    public String getAuthenticated() {
        return this.authenticated;
    }

    public void setAuthenticated(String authenticated) {
        this.authenticated = authenticated;
    }

    @Column(name = "`IntCol UserId`", nullable = true, scale = 0, precision = 10)
    public Integer getIntColUserId() {
        return this.intColUserId;
    }

    public void setIntColUserId(Integer intColUserId) {
        this.intColUserId = intColUserId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ServerDefinedIu)) return false;
        final ServerDefinedIu serverDefinedIu = (ServerDefinedIu) o;
        return Objects.equals(getIntegerCol(), serverDefinedIu.getIntegerCol()) &&
                Objects.equals(getBigIntegerCol(), serverDefinedIu.getBigIntegerCol());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIntegerCol(),
                getBigIntegerCol());
    }
}

