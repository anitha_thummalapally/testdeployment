/*Copyright (c) 2016-2017 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.testdeployment.testdefaultvalues;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

public class Trigger1Id implements Serializable {

    private String stringcol;
    private Integer intcol;

    public String getStringcol() {
        return this.stringcol;
    }

    public void setStringcol(String stringcol) {
        this.stringcol = stringcol;
    }

    public Integer getIntcol() {
        return this.intcol;
    }

    public void setIntcol(Integer intcol) {
        this.intcol = intcol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Trigger1)) return false;
        final Trigger1 trigger1 = (Trigger1) o;
        return Objects.equals(getStringcol(), trigger1.getStringcol()) &&
                Objects.equals(getIntcol(), trigger1.getIntcol());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getStringcol(),
                getIntcol());
    }
}
