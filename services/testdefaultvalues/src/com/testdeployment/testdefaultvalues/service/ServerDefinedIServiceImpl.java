/*Copyright (c) 2016-2017 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.testdeployment.testdefaultvalues.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.testdeployment.testdefaultvalues.ServerDefinedI;
import com.testdeployment.testdefaultvalues.ServerDefinedIId;


/**
 * ServiceImpl object for domain model class ServerDefinedI.
 *
 * @see ServerDefinedI
 */
@Service("testdefaultvalues.ServerDefinedIService")
@Validated
public class ServerDefinedIServiceImpl implements ServerDefinedIService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServerDefinedIServiceImpl.class);


    @Autowired
    @Qualifier("testdefaultvalues.ServerDefinedIDao")
    private WMGenericDao<ServerDefinedI, ServerDefinedIId> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<ServerDefinedI, ServerDefinedIId> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "testdefaultvaluesTransactionManager")
    @Override
	public ServerDefinedI create(ServerDefinedI serverDefinedI) {
        LOGGER.debug("Creating a new ServerDefinedI with information: {}", serverDefinedI);
        ServerDefinedI serverDefinedICreated = this.wmGenericDao.create(serverDefinedI);
        return serverDefinedICreated;
    }

	@Transactional(readOnly = true, value = "testdefaultvaluesTransactionManager")
	@Override
	public ServerDefinedI getById(ServerDefinedIId serverdefinediId) throws EntityNotFoundException {
        LOGGER.debug("Finding ServerDefinedI by id: {}", serverdefinediId);
        ServerDefinedI serverDefinedI = this.wmGenericDao.findById(serverdefinediId);
        if (serverDefinedI == null){
            LOGGER.debug("No ServerDefinedI found with id: {}", serverdefinediId);
            throw new EntityNotFoundException(String.valueOf(serverdefinediId));
        }
        return serverDefinedI;
    }

    @Transactional(readOnly = true, value = "testdefaultvaluesTransactionManager")
	@Override
	public ServerDefinedI findById(ServerDefinedIId serverdefinediId) {
        LOGGER.debug("Finding ServerDefinedI by id: {}", serverdefinediId);
        return this.wmGenericDao.findById(serverdefinediId);
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "testdefaultvaluesTransactionManager")
	@Override
	public ServerDefinedI update(ServerDefinedI serverDefinedI) throws EntityNotFoundException {
        LOGGER.debug("Updating ServerDefinedI with information: {}", serverDefinedI);
        this.wmGenericDao.update(serverDefinedI);

        ServerDefinedIId serverdefinediId = new ServerDefinedIId();
        serverdefinediId.setDoubleCol(serverDefinedI.getDoubleCol());
        serverdefinediId.setFloatCol(serverDefinedI.getFloatCol());

        return this.wmGenericDao.findById(serverdefinediId);
    }

    @Transactional(value = "testdefaultvaluesTransactionManager")
	@Override
	public ServerDefinedI delete(ServerDefinedIId serverdefinediId) throws EntityNotFoundException {
        LOGGER.debug("Deleting ServerDefinedI with id: {}", serverdefinediId);
        ServerDefinedI deleted = this.wmGenericDao.findById(serverdefinediId);
        if (deleted == null) {
            LOGGER.debug("No ServerDefinedI found with id: {}", serverdefinediId);
            throw new EntityNotFoundException(String.valueOf(serverdefinediId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "testdefaultvaluesTransactionManager")
	@Override
	public Page<ServerDefinedI> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all ServerDefinedIs");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "testdefaultvaluesTransactionManager")
    @Override
    public Page<ServerDefinedI> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all ServerDefinedIs");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "testdefaultvaluesTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service testdefaultvalues for table ServerDefinedI to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "testdefaultvaluesTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "testdefaultvaluesTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

