/*Copyright (c) 2016-2017 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.testdeployment.testdefaultvalues.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.testdeployment.testdefaultvalues.DatabaseDefined;


/**
 * ServiceImpl object for domain model class DatabaseDefined.
 *
 * @see DatabaseDefined
 */
@Service("testdefaultvalues.DatabaseDefinedService")
@Validated
public class DatabaseDefinedServiceImpl implements DatabaseDefinedService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseDefinedServiceImpl.class);


    @Autowired
    @Qualifier("testdefaultvalues.DatabaseDefinedDao")
    private WMGenericDao<DatabaseDefined, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<DatabaseDefined, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "testdefaultvaluesTransactionManager")
    @Override
	public DatabaseDefined create(DatabaseDefined databaseDefined) {
        LOGGER.debug("Creating a new DatabaseDefined with information: {}", databaseDefined);
        DatabaseDefined databaseDefinedCreated = this.wmGenericDao.create(databaseDefined);
        return databaseDefinedCreated;
    }

	@Transactional(readOnly = true, value = "testdefaultvaluesTransactionManager")
	@Override
	public DatabaseDefined getById(Integer databasedefinedId) throws EntityNotFoundException {
        LOGGER.debug("Finding DatabaseDefined by id: {}", databasedefinedId);
        DatabaseDefined databaseDefined = this.wmGenericDao.findById(databasedefinedId);
        if (databaseDefined == null){
            LOGGER.debug("No DatabaseDefined found with id: {}", databasedefinedId);
            throw new EntityNotFoundException(String.valueOf(databasedefinedId));
        }
        return databaseDefined;
    }

    @Transactional(readOnly = true, value = "testdefaultvaluesTransactionManager")
	@Override
	public DatabaseDefined findById(Integer databasedefinedId) {
        LOGGER.debug("Finding DatabaseDefined by id: {}", databasedefinedId);
        return this.wmGenericDao.findById(databasedefinedId);
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "testdefaultvaluesTransactionManager")
	@Override
	public DatabaseDefined update(DatabaseDefined databaseDefined) throws EntityNotFoundException {
        LOGGER.debug("Updating DatabaseDefined with information: {}", databaseDefined);
        this.wmGenericDao.update(databaseDefined);

        Integer databasedefinedId = databaseDefined.getIntegerCol();

        return this.wmGenericDao.findById(databasedefinedId);
    }

    @Transactional(value = "testdefaultvaluesTransactionManager")
	@Override
	public DatabaseDefined delete(Integer databasedefinedId) throws EntityNotFoundException {
        LOGGER.debug("Deleting DatabaseDefined with id: {}", databasedefinedId);
        DatabaseDefined deleted = this.wmGenericDao.findById(databasedefinedId);
        if (deleted == null) {
            LOGGER.debug("No DatabaseDefined found with id: {}", databasedefinedId);
            throw new EntityNotFoundException(String.valueOf(databasedefinedId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "testdefaultvaluesTransactionManager")
	@Override
	public Page<DatabaseDefined> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all DatabaseDefineds");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "testdefaultvaluesTransactionManager")
    @Override
    public Page<DatabaseDefined> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all DatabaseDefineds");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "testdefaultvaluesTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service testdefaultvalues for table DatabaseDefined to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "testdefaultvaluesTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "testdefaultvaluesTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

