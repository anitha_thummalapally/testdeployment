/*Copyright (c) 2016-2017 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.testdeployment.testdefaultvalues.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.testdeployment.testdefaultvalues.Test2;

/**
 * Service object for domain model class {@link Test2}.
 */
public interface Test2Service {

    /**
     * Creates a new Test2. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on Test2 if any.
     *
     * @param test2 Details of the Test2 to be created; value cannot be null.
     * @return The newly created Test2.
     */
	Test2 create(@Valid Test2 test2);


	/**
	 * Returns Test2 by given id if exists.
	 *
	 * @param test2Id The id of the Test2 to get; value cannot be null.
	 * @return Test2 associated with the given test2Id.
     * @throws EntityNotFoundException If no Test2 is found.
	 */
	Test2 getById(Integer test2Id) throws EntityNotFoundException;

    /**
	 * Find and return the Test2 by given id if exists, returns null otherwise.
	 *
	 * @param test2Id The id of the Test2 to get; value cannot be null.
	 * @return Test2 associated with the given test2Id.
	 */
	Test2 findById(Integer test2Id);


	/**
	 * Updates the details of an existing Test2. It replaces all fields of the existing Test2 with the given test2.
	 *
     * This method overrides the input field values using Server side or database managed properties defined on Test2 if any.
     *
	 * @param test2 The details of the Test2 to be updated; value cannot be null.
	 * @return The updated Test2.
	 * @throws EntityNotFoundException if no Test2 is found with given input.
	 */
	Test2 update(@Valid Test2 test2) throws EntityNotFoundException;

    /**
	 * Deletes an existing Test2 with the given id.
	 *
	 * @param test2Id The id of the Test2 to be deleted; value cannot be null.
	 * @return The deleted Test2.
	 * @throws EntityNotFoundException if no Test2 found with the given id.
	 */
	Test2 delete(Integer test2Id) throws EntityNotFoundException;

	/**
	 * Find all Test2s matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
	 *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
	 *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Test2s.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
	 */
    @Deprecated
	Page<Test2> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Find all Test2s matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Test2s.
     *
     * @see Pageable
     * @see Page
	 */
    Page<Test2> findAll(String query, Pageable pageable);

    /**
	 * Exports all Test2s matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
	 */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the Test2s in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
	 * @return The count of the Test2.
	 */
	long count(String query);

	/**
	 * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
	 * @return Paginated data with included fields.

     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
	Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);


}

