/*Copyright (c) 2016-2017 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.testdeployment.testdefaultvalues.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.testdeployment.testdefaultvalues.Test2;


/**
 * ServiceImpl object for domain model class Test2.
 *
 * @see Test2
 */
@Service("testdefaultvalues.Test2Service")
@Validated
public class Test2ServiceImpl implements Test2Service {

    private static final Logger LOGGER = LoggerFactory.getLogger(Test2ServiceImpl.class);


    @Autowired
    @Qualifier("testdefaultvalues.Test2Dao")
    private WMGenericDao<Test2, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Test2, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "testdefaultvaluesTransactionManager")
    @Override
	public Test2 create(Test2 test2) {
        LOGGER.debug("Creating a new Test2 with information: {}", test2);
        Test2 test2Created = this.wmGenericDao.create(test2);
        return test2Created;
    }

	@Transactional(readOnly = true, value = "testdefaultvaluesTransactionManager")
	@Override
	public Test2 getById(Integer test2Id) throws EntityNotFoundException {
        LOGGER.debug("Finding Test2 by id: {}", test2Id);
        Test2 test2 = this.wmGenericDao.findById(test2Id);
        if (test2 == null){
            LOGGER.debug("No Test2 found with id: {}", test2Id);
            throw new EntityNotFoundException(String.valueOf(test2Id));
        }
        return test2;
    }

    @Transactional(readOnly = true, value = "testdefaultvaluesTransactionManager")
	@Override
	public Test2 findById(Integer test2Id) {
        LOGGER.debug("Finding Test2 by id: {}", test2Id);
        return this.wmGenericDao.findById(test2Id);
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "testdefaultvaluesTransactionManager")
	@Override
	public Test2 update(Test2 test2) throws EntityNotFoundException {
        LOGGER.debug("Updating Test2 with information: {}", test2);
        this.wmGenericDao.update(test2);

        Integer test2Id = test2.getA2();

        return this.wmGenericDao.findById(test2Id);
    }

    @Transactional(value = "testdefaultvaluesTransactionManager")
	@Override
	public Test2 delete(Integer test2Id) throws EntityNotFoundException {
        LOGGER.debug("Deleting Test2 with id: {}", test2Id);
        Test2 deleted = this.wmGenericDao.findById(test2Id);
        if (deleted == null) {
            LOGGER.debug("No Test2 found with id: {}", test2Id);
            throw new EntityNotFoundException(String.valueOf(test2Id));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "testdefaultvaluesTransactionManager")
	@Override
	public Page<Test2> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Test2s");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "testdefaultvaluesTransactionManager")
    @Override
    public Page<Test2> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Test2s");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "testdefaultvaluesTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service testdefaultvalues for table Test2 to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "testdefaultvaluesTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "testdefaultvaluesTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

