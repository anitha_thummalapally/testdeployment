/*Copyright (c) 2016-2017 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.testdeployment.testdefaultvalues.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.testdeployment.testdefaultvalues.TestDefValue;

/**
 * Service object for domain model class {@link TestDefValue}.
 */
public interface TestDefValueService {

    /**
     * Creates a new TestDefValue. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on TestDefValue if any.
     *
     * @param testDefValue Details of the TestDefValue to be created; value cannot be null.
     * @return The newly created TestDefValue.
     */
	TestDefValue create(@Valid TestDefValue testDefValue);


	/**
	 * Returns TestDefValue by given id if exists.
	 *
	 * @param testdefvalueId The id of the TestDefValue to get; value cannot be null.
	 * @return TestDefValue associated with the given testdefvalueId.
     * @throws EntityNotFoundException If no TestDefValue is found.
	 */
	TestDefValue getById(Integer testdefvalueId) throws EntityNotFoundException;

    /**
	 * Find and return the TestDefValue by given id if exists, returns null otherwise.
	 *
	 * @param testdefvalueId The id of the TestDefValue to get; value cannot be null.
	 * @return TestDefValue associated with the given testdefvalueId.
	 */
	TestDefValue findById(Integer testdefvalueId);


	/**
	 * Updates the details of an existing TestDefValue. It replaces all fields of the existing TestDefValue with the given testDefValue.
	 *
     * This method overrides the input field values using Server side or database managed properties defined on TestDefValue if any.
     *
	 * @param testDefValue The details of the TestDefValue to be updated; value cannot be null.
	 * @return The updated TestDefValue.
	 * @throws EntityNotFoundException if no TestDefValue is found with given input.
	 */
	TestDefValue update(@Valid TestDefValue testDefValue) throws EntityNotFoundException;

    /**
	 * Deletes an existing TestDefValue with the given id.
	 *
	 * @param testdefvalueId The id of the TestDefValue to be deleted; value cannot be null.
	 * @return The deleted TestDefValue.
	 * @throws EntityNotFoundException if no TestDefValue found with the given id.
	 */
	TestDefValue delete(Integer testdefvalueId) throws EntityNotFoundException;

	/**
	 * Find all TestDefValues matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
	 *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
	 *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching TestDefValues.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
	 */
    @Deprecated
	Page<TestDefValue> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Find all TestDefValues matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching TestDefValues.
     *
     * @see Pageable
     * @see Page
	 */
    Page<TestDefValue> findAll(String query, Pageable pageable);

    /**
	 * Exports all TestDefValues matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
	 */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the TestDefValues in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
	 * @return The count of the TestDefValue.
	 */
	long count(String query);

	/**
	 * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
	 * @return Paginated data with included fields.

     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
	Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);


}

