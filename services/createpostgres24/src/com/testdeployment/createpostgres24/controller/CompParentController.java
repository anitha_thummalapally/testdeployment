/*Copyright (c) 2016-2017 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.testdeployment.createpostgres24.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.testdeployment.createpostgres24.CompChild;
import com.testdeployment.createpostgres24.CompParent;
import com.testdeployment.createpostgres24.CompParentId;
import com.testdeployment.createpostgres24.service.CompParentService;


/**
 * Controller object for domain model class CompParent.
 * @see CompParent
 */
@RestController("createpostgres24.CompParentController")
@Api(value = "CompParentController", description = "Exposes APIs to work with CompParent resource.")
@RequestMapping("/createpostgres24/CompParent")
public class CompParentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompParentController.class);

    @Autowired
	@Qualifier("createpostgres24.CompParentService")
	private CompParentService compParentService;

	@ApiOperation(value = "Creates a new CompParent instance.")
@RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
public CompParent createCompParent(@RequestBody CompParent compParent) {
		LOGGER.debug("Create CompParent with information: {}" , compParent);

		compParent = compParentService.create(compParent);
		LOGGER.debug("Created CompParent with information: {}" , compParent);

	    return compParent;
	}

@ApiOperation(value = "Returns the CompParent instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public CompParent getCompParent(@RequestParam("compName") String compName,@RequestParam("compId") Integer compId) throws EntityNotFoundException {

        CompParentId compparentId = new CompParentId();
        compparentId.setCompName(compName);
        compparentId.setCompId(compId);

        LOGGER.debug("Getting CompParent with id: {}" , compparentId);
        CompParent compParent = compParentService.getById(compparentId);
        LOGGER.debug("CompParent details with id: {}" , compParent);

        return compParent;
    }



    @ApiOperation(value = "Updates the CompParent instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public CompParent editCompParent(@RequestParam("compName") String compName,@RequestParam("compId") Integer compId, @RequestBody CompParent compParent) throws EntityNotFoundException {

        compParent.setCompName(compName);
        compParent.setCompId(compId);

        LOGGER.debug("CompParent details with id is updated with: {}" , compParent);

        return compParentService.update(compParent);
    }


    @ApiOperation(value = "Deletes the CompParent instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteCompParent(@RequestParam("compName") String compName,@RequestParam("compId") Integer compId) throws EntityNotFoundException {

        CompParentId compparentId = new CompParentId();
        compparentId.setCompName(compName);
        compparentId.setCompId(compId);

        LOGGER.debug("Deleting CompParent with id: {}" , compparentId);
        CompParent compParent = compParentService.delete(compparentId);

        return compParent != null;
    }


    /**
     * @deprecated Use {@link #findCompParents(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of CompParent instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<CompParent> searchCompParentsByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering CompParents list");
        return compParentService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of CompParent instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<CompParent> findCompParents(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering CompParents list");
        return compParentService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of CompParent instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<CompParent> filterCompParents(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering CompParents list");
        return compParentService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportCompParents(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return compParentService.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of CompParent instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countCompParents( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting CompParents");
		return compParentService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getCompParentAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return compParentService.getAggregatedValues(aggregationInfo, pageable);
    }

    @RequestMapping(value="/composite-id/compChilds", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the compChilds instance associated with the given composite-id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<CompChild> findAssociatedCompChilds(@RequestParam("compName") String compName,@RequestParam("compId") Integer compId, Pageable pageable) {

        LOGGER.debug("Fetching all associated compChilds");
        return compParentService.findAssociatedCompChilds(compName, compId, pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service CompParentService instance
	 */
	protected void setCompParentService(CompParentService service) {
		this.compParentService = service;
	}

}

