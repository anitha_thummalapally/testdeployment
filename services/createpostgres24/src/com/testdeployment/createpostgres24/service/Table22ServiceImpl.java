/*Copyright (c) 2016-2017 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.testdeployment.createpostgres24.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.testdeployment.createpostgres24.Table22;


/**
 * ServiceImpl object for domain model class Table22.
 *
 * @see Table22
 */
@Service("createpostgres24.Table22Service")
@Validated
public class Table22ServiceImpl implements Table22Service {

    private static final Logger LOGGER = LoggerFactory.getLogger(Table22ServiceImpl.class);


    @Autowired
    @Qualifier("createpostgres24.Table22Dao")
    private WMGenericDao<Table22, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Table22, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "createpostgres24TransactionManager")
    @Override
	public Table22 create(Table22 table22) {
        LOGGER.debug("Creating a new Table22 with information: {}", table22);
        Table22 table22Created = this.wmGenericDao.create(table22);
        return table22Created;
    }

	@Transactional(readOnly = true, value = "createpostgres24TransactionManager")
	@Override
	public Table22 getById(Integer table22Id) throws EntityNotFoundException {
        LOGGER.debug("Finding Table22 by id: {}", table22Id);
        Table22 table22 = this.wmGenericDao.findById(table22Id);
        if (table22 == null){
            LOGGER.debug("No Table22 found with id: {}", table22Id);
            throw new EntityNotFoundException(String.valueOf(table22Id));
        }
        return table22;
    }

    @Transactional(readOnly = true, value = "createpostgres24TransactionManager")
	@Override
	public Table22 findById(Integer table22Id) {
        LOGGER.debug("Finding Table22 by id: {}", table22Id);
        return this.wmGenericDao.findById(table22Id);
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "createpostgres24TransactionManager")
	@Override
	public Table22 update(Table22 table22) throws EntityNotFoundException {
        LOGGER.debug("Updating Table22 with information: {}", table22);
        this.wmGenericDao.update(table22);

        Integer table22Id = table22.getId();

        return this.wmGenericDao.findById(table22Id);
    }

    @Transactional(value = "createpostgres24TransactionManager")
	@Override
	public Table22 delete(Integer table22Id) throws EntityNotFoundException {
        LOGGER.debug("Deleting Table22 with id: {}", table22Id);
        Table22 deleted = this.wmGenericDao.findById(table22Id);
        if (deleted == null) {
            LOGGER.debug("No Table22 found with id: {}", table22Id);
            throw new EntityNotFoundException(String.valueOf(table22Id));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "createpostgres24TransactionManager")
	@Override
	public Page<Table22> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Table22s");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "createpostgres24TransactionManager")
    @Override
    public Page<Table22> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Table22s");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "createpostgres24TransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service createpostgres24 for table Table22 to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "createpostgres24TransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "createpostgres24TransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

