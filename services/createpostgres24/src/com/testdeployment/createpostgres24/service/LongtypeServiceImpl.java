/*Copyright (c) 2016-2017 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.testdeployment.createpostgres24.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.testdeployment.createpostgres24.Longtype;


/**
 * ServiceImpl object for domain model class Longtype.
 *
 * @see Longtype
 */
@Service("createpostgres24.LongtypeService")
@Validated
public class LongtypeServiceImpl implements LongtypeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LongtypeServiceImpl.class);


    @Autowired
    @Qualifier("createpostgres24.LongtypeDao")
    private WMGenericDao<Longtype, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Longtype, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "createpostgres24TransactionManager")
    @Override
	public Longtype create(Longtype longtype) {
        LOGGER.debug("Creating a new Longtype with information: {}", longtype);
        Longtype longtypeCreated = this.wmGenericDao.create(longtype);
        return longtypeCreated;
    }

	@Transactional(readOnly = true, value = "createpostgres24TransactionManager")
	@Override
	public Longtype getById(Integer longtypeId) throws EntityNotFoundException {
        LOGGER.debug("Finding Longtype by id: {}", longtypeId);
        Longtype longtype = this.wmGenericDao.findById(longtypeId);
        if (longtype == null){
            LOGGER.debug("No Longtype found with id: {}", longtypeId);
            throw new EntityNotFoundException(String.valueOf(longtypeId));
        }
        return longtype;
    }

    @Transactional(readOnly = true, value = "createpostgres24TransactionManager")
	@Override
	public Longtype findById(Integer longtypeId) {
        LOGGER.debug("Finding Longtype by id: {}", longtypeId);
        return this.wmGenericDao.findById(longtypeId);
    }

    @Transactional(readOnly = true, value = "createpostgres24TransactionManager")
    @Override
    public Longtype getByIdAndLongcolumn(int id, Long longcolumn) {
        Map<String, Object> idAndLongcolumnMap = new HashMap<>();
        idAndLongcolumnMap.put("id", id);
        idAndLongcolumnMap.put("longcolumn", longcolumn);

        LOGGER.debug("Finding Longtype by unique keys: {}", idAndLongcolumnMap);
        Longtype longtype = this.wmGenericDao.findByUniqueKey(idAndLongcolumnMap);

        if (longtype == null){
            LOGGER.debug("No Longtype found with given unique key values: {}", idAndLongcolumnMap);
            throw new EntityNotFoundException(String.valueOf(idAndLongcolumnMap));
        }

        return longtype;
    }

	@Transactional(rollbackFor = EntityNotFoundException.class, value = "createpostgres24TransactionManager")
	@Override
	public Longtype update(Longtype longtype) throws EntityNotFoundException {
        LOGGER.debug("Updating Longtype with information: {}", longtype);
        this.wmGenericDao.update(longtype);

        Integer longtypeId = longtype.getId();

        return this.wmGenericDao.findById(longtypeId);
    }

    @Transactional(value = "createpostgres24TransactionManager")
	@Override
	public Longtype delete(Integer longtypeId) throws EntityNotFoundException {
        LOGGER.debug("Deleting Longtype with id: {}", longtypeId);
        Longtype deleted = this.wmGenericDao.findById(longtypeId);
        if (deleted == null) {
            LOGGER.debug("No Longtype found with id: {}", longtypeId);
            throw new EntityNotFoundException(String.valueOf(longtypeId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "createpostgres24TransactionManager")
	@Override
	public Page<Longtype> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Longtypes");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "createpostgres24TransactionManager")
    @Override
    public Page<Longtype> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Longtypes");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "createpostgres24TransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service createpostgres24 for table Longtype to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "createpostgres24TransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "createpostgres24TransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

