/*Copyright (c) 2016-2017 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.testdeployment.createpostgres24.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.testdeployment.createpostgres24.Table321;

/**
 * Service object for domain model class {@link Table321}.
 */
public interface Table321Service {

    /**
     * Creates a new Table321. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on Table321 if any.
     *
     * @param table321 Details of the Table321 to be created; value cannot be null.
     * @return The newly created Table321.
     */
	Table321 create(@Valid Table321 table321);


	/**
	 * Returns Table321 by given id if exists.
	 *
	 * @param table321Id The id of the Table321 to get; value cannot be null.
	 * @return Table321 associated with the given table321Id.
     * @throws EntityNotFoundException If no Table321 is found.
	 */
	Table321 getById(Integer table321Id) throws EntityNotFoundException;

    /**
	 * Find and return the Table321 by given id if exists, returns null otherwise.
	 *
	 * @param table321Id The id of the Table321 to get; value cannot be null.
	 * @return Table321 associated with the given table321Id.
	 */
	Table321 findById(Integer table321Id);


	/**
	 * Updates the details of an existing Table321. It replaces all fields of the existing Table321 with the given table321.
	 *
     * This method overrides the input field values using Server side or database managed properties defined on Table321 if any.
     *
	 * @param table321 The details of the Table321 to be updated; value cannot be null.
	 * @return The updated Table321.
	 * @throws EntityNotFoundException if no Table321 is found with given input.
	 */
	Table321 update(@Valid Table321 table321) throws EntityNotFoundException;

    /**
	 * Deletes an existing Table321 with the given id.
	 *
	 * @param table321Id The id of the Table321 to be deleted; value cannot be null.
	 * @return The deleted Table321.
	 * @throws EntityNotFoundException if no Table321 found with the given id.
	 */
	Table321 delete(Integer table321Id) throws EntityNotFoundException;

	/**
	 * Find all Table321s matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
	 *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
	 *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Table321s.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
	 */
    @Deprecated
	Page<Table321> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Find all Table321s matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Table321s.
     *
     * @see Pageable
     * @see Page
	 */
    Page<Table321> findAll(String query, Pageable pageable);

    /**
	 * Exports all Table321s matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
	 */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the Table321s in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
	 * @return The count of the Table321.
	 */
	long count(String query);

	/**
	 * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
	 * @return Paginated data with included fields.

     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
	Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);


}

