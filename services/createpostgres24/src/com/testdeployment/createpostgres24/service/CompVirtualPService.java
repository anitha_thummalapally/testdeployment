/*Copyright (c) 2016-2017 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.testdeployment.createpostgres24.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.testdeployment.createpostgres24.CompVirtualP;
import com.testdeployment.createpostgres24.CompVirtualPId;

/**
 * Service object for domain model class {@link CompVirtualP}.
 */
public interface CompVirtualPService {

    /**
     * Creates a new CompVirtualP. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on CompVirtualP if any.
     *
     * @param compVirtualP Details of the CompVirtualP to be created; value cannot be null.
     * @return The newly created CompVirtualP.
     */
	CompVirtualP create(@Valid CompVirtualP compVirtualP);


	/**
	 * Returns CompVirtualP by given id if exists.
	 *
	 * @param compvirtualpId The id of the CompVirtualP to get; value cannot be null.
	 * @return CompVirtualP associated with the given compvirtualpId.
     * @throws EntityNotFoundException If no CompVirtualP is found.
	 */
	CompVirtualP getById(CompVirtualPId compvirtualpId) throws EntityNotFoundException;

    /**
	 * Find and return the CompVirtualP by given id if exists, returns null otherwise.
	 *
	 * @param compvirtualpId The id of the CompVirtualP to get; value cannot be null.
	 * @return CompVirtualP associated with the given compvirtualpId.
	 */
	CompVirtualP findById(CompVirtualPId compvirtualpId);


	/**
	 * Updates the details of an existing CompVirtualP. It replaces all fields of the existing CompVirtualP with the given compVirtualP.
	 *
     * This method overrides the input field values using Server side or database managed properties defined on CompVirtualP if any.
     *
	 * @param compVirtualP The details of the CompVirtualP to be updated; value cannot be null.
	 * @return The updated CompVirtualP.
	 * @throws EntityNotFoundException if no CompVirtualP is found with given input.
	 */
	CompVirtualP update(@Valid CompVirtualP compVirtualP) throws EntityNotFoundException;

    /**
	 * Deletes an existing CompVirtualP with the given id.
	 *
	 * @param compvirtualpId The id of the CompVirtualP to be deleted; value cannot be null.
	 * @return The deleted CompVirtualP.
	 * @throws EntityNotFoundException if no CompVirtualP found with the given id.
	 */
	CompVirtualP delete(CompVirtualPId compvirtualpId) throws EntityNotFoundException;

	/**
	 * Find all CompVirtualPs matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
	 *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
	 *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching CompVirtualPs.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
	 */
    @Deprecated
	Page<CompVirtualP> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Find all CompVirtualPs matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching CompVirtualPs.
     *
     * @see Pageable
     * @see Page
	 */
    Page<CompVirtualP> findAll(String query, Pageable pageable);

    /**
	 * Exports all CompVirtualPs matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
	 */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the CompVirtualPs in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
	 * @return The count of the CompVirtualP.
	 */
	long count(String query);

	/**
	 * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
	 * @return Paginated data with included fields.

     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
	Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);


}

