/*Copyright (c) 2016-2017 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.testdeployment.createpostgres24.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.testdeployment.createpostgres24.CompVirtualC;
import com.testdeployment.createpostgres24.CompVirtualCId;


/**
 * ServiceImpl object for domain model class CompVirtualC.
 *
 * @see CompVirtualC
 */
@Service("createpostgres24.CompVirtualCService")
@Validated
public class CompVirtualCServiceImpl implements CompVirtualCService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompVirtualCServiceImpl.class);


    @Autowired
    @Qualifier("createpostgres24.CompVirtualCDao")
    private WMGenericDao<CompVirtualC, CompVirtualCId> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<CompVirtualC, CompVirtualCId> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "createpostgres24TransactionManager")
    @Override
	public CompVirtualC create(CompVirtualC compVirtualC) {
        LOGGER.debug("Creating a new CompVirtualC with information: {}", compVirtualC);
        CompVirtualC compVirtualCCreated = this.wmGenericDao.create(compVirtualC);
        return compVirtualCCreated;
    }

	@Transactional(readOnly = true, value = "createpostgres24TransactionManager")
	@Override
	public CompVirtualC getById(CompVirtualCId compvirtualcId) throws EntityNotFoundException {
        LOGGER.debug("Finding CompVirtualC by id: {}", compvirtualcId);
        CompVirtualC compVirtualC = this.wmGenericDao.findById(compvirtualcId);
        if (compVirtualC == null){
            LOGGER.debug("No CompVirtualC found with id: {}", compvirtualcId);
            throw new EntityNotFoundException(String.valueOf(compvirtualcId));
        }
        return compVirtualC;
    }

    @Transactional(readOnly = true, value = "createpostgres24TransactionManager")
	@Override
	public CompVirtualC findById(CompVirtualCId compvirtualcId) {
        LOGGER.debug("Finding CompVirtualC by id: {}", compvirtualcId);
        return this.wmGenericDao.findById(compvirtualcId);
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "createpostgres24TransactionManager")
	@Override
	public CompVirtualC update(CompVirtualC compVirtualC) throws EntityNotFoundException {
        LOGGER.debug("Updating CompVirtualC with information: {}", compVirtualC);
        this.wmGenericDao.update(compVirtualC);

        CompVirtualCId compvirtualcId = new CompVirtualCId();
        compvirtualcId.setVirtualCompid(compVirtualC.getVirtualCompid());
        compvirtualcId.setVirtualCompName(compVirtualC.getVirtualCompName());
        compvirtualcId.setVirtualCompDesc(compVirtualC.getVirtualCompDesc());

        return this.wmGenericDao.findById(compvirtualcId);
    }

    @Transactional(value = "createpostgres24TransactionManager")
	@Override
	public CompVirtualC delete(CompVirtualCId compvirtualcId) throws EntityNotFoundException {
        LOGGER.debug("Deleting CompVirtualC with id: {}", compvirtualcId);
        CompVirtualC deleted = this.wmGenericDao.findById(compvirtualcId);
        if (deleted == null) {
            LOGGER.debug("No CompVirtualC found with id: {}", compvirtualcId);
            throw new EntityNotFoundException(String.valueOf(compvirtualcId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "createpostgres24TransactionManager")
	@Override
	public Page<CompVirtualC> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all CompVirtualCs");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "createpostgres24TransactionManager")
    @Override
    public Page<CompVirtualC> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all CompVirtualCs");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "createpostgres24TransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service createpostgres24 for table CompVirtualC to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "createpostgres24TransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "createpostgres24TransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

