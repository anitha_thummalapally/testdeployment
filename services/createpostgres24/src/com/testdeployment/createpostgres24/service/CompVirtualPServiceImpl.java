/*Copyright (c) 2016-2017 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.testdeployment.createpostgres24.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.testdeployment.createpostgres24.CompVirtualP;
import com.testdeployment.createpostgres24.CompVirtualPId;


/**
 * ServiceImpl object for domain model class CompVirtualP.
 *
 * @see CompVirtualP
 */
@Service("createpostgres24.CompVirtualPService")
@Validated
public class CompVirtualPServiceImpl implements CompVirtualPService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompVirtualPServiceImpl.class);


    @Autowired
    @Qualifier("createpostgres24.CompVirtualPDao")
    private WMGenericDao<CompVirtualP, CompVirtualPId> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<CompVirtualP, CompVirtualPId> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "createpostgres24TransactionManager")
    @Override
	public CompVirtualP create(CompVirtualP compVirtualP) {
        LOGGER.debug("Creating a new CompVirtualP with information: {}", compVirtualP);
        CompVirtualP compVirtualPCreated = this.wmGenericDao.create(compVirtualP);
        return compVirtualPCreated;
    }

	@Transactional(readOnly = true, value = "createpostgres24TransactionManager")
	@Override
	public CompVirtualP getById(CompVirtualPId compvirtualpId) throws EntityNotFoundException {
        LOGGER.debug("Finding CompVirtualP by id: {}", compvirtualpId);
        CompVirtualP compVirtualP = this.wmGenericDao.findById(compvirtualpId);
        if (compVirtualP == null){
            LOGGER.debug("No CompVirtualP found with id: {}", compvirtualpId);
            throw new EntityNotFoundException(String.valueOf(compvirtualpId));
        }
        return compVirtualP;
    }

    @Transactional(readOnly = true, value = "createpostgres24TransactionManager")
	@Override
	public CompVirtualP findById(CompVirtualPId compvirtualpId) {
        LOGGER.debug("Finding CompVirtualP by id: {}", compvirtualpId);
        return this.wmGenericDao.findById(compvirtualpId);
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "createpostgres24TransactionManager")
	@Override
	public CompVirtualP update(CompVirtualP compVirtualP) throws EntityNotFoundException {
        LOGGER.debug("Updating CompVirtualP with information: {}", compVirtualP);
        this.wmGenericDao.update(compVirtualP);

        CompVirtualPId compvirtualpId = new CompVirtualPId();
        compvirtualpId.setVirtualCompname(compVirtualP.getVirtualCompname());
        compvirtualpId.setVirtualCompid(compVirtualP.getVirtualCompid());

        return this.wmGenericDao.findById(compvirtualpId);
    }

    @Transactional(value = "createpostgres24TransactionManager")
	@Override
	public CompVirtualP delete(CompVirtualPId compvirtualpId) throws EntityNotFoundException {
        LOGGER.debug("Deleting CompVirtualP with id: {}", compvirtualpId);
        CompVirtualP deleted = this.wmGenericDao.findById(compvirtualpId);
        if (deleted == null) {
            LOGGER.debug("No CompVirtualP found with id: {}", compvirtualpId);
            throw new EntityNotFoundException(String.valueOf(compvirtualpId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "createpostgres24TransactionManager")
	@Override
	public Page<CompVirtualP> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all CompVirtualPs");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "createpostgres24TransactionManager")
    @Override
    public Page<CompVirtualP> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all CompVirtualPs");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "createpostgres24TransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service createpostgres24 for table CompVirtualP to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "createpostgres24TransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "createpostgres24TransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

