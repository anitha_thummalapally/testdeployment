var _WM_APP_PROPERTIES = {
  "activeTheme" : "material",
  "dateFormat" : "",
  "defaultLanguage" : "en",
  "displayName" : "TestDeployment",
  "homePage" : "Main",
  "name" : "TestDeployment",
  "platformType" : "WEB",
  "securityEnabled" : "true",
  "supportedLanguages" : "en",
  "timeFormat" : "",
  "type" : "APPLICATION",
  "version" : "1.0"
};